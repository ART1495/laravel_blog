<?php

namespace App\Http\Controllers;

use App\User;

class RegistrationController extends Controller
{
    public function create(){
    	return view('sessions.create'); 
    }

    public function store(){

    	// Validate data
    	$this->validate(request(), [
    		'name' => 'required',
    		'email' => 'required|email',
    		'password' => 'required|confirmed'
    	]);

    	// Create and save User
    	$user = User::create(request(['name', 'email', 'password']));

    	// Sign in
    	auth()->login($user);

    	return redirect()->home();
    }
}
