<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function create(){
    	return redirect('/logout');
    }

    public function destroy(){
    	auth()->logout();

    	return redirect()->home();
    }
}
