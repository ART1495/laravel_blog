<?php

namespace App\Http\Controllers;

use App\Post;

class PostsController extends Controller {
    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index() {
        $posts = Post::latest()->get();
    	return view('posts.index', compact('posts'));
    }

    public function show(Post $post){
    	return view('posts.show', compact('post'));
    }

    public function create(){
    	return view('posts.create');
    }

    public function store(){
    	// $post = new Post;	 Simillar to ... = new \App\Post if 'Use App\Post;' not declared

    	// the FF. coressponds to the fields.
    	// $post->title = request('title');
    	// $post->body = request('body');

    	// Save to DB
    	// $post->save();

        // Form Validation
        $this->validate(request(), [
                'title' => 'required',
                'body'  => 'required'
            ]);

    	Post::create(request([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->id()
        ]));

    	// Redirect to Homepage
    	return redirect('/');
    }
}
