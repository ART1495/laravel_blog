<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
	// $fillable = Accepts input to the fields specified.
	// $guarded = Opposite of $fillable
    protected $guarded = [];
}
